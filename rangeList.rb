# -*- coding : utf-8 -*-

=begin
将range的左端、右端分别存储在left和right数组中，例如 [1,5],[10,11] 存储形式为：
left    right
1       5
10      11
在进行插入删除时，分别在left，right数组寻找操作的位置，例如在 [1 , 5],[11 , 20] 中插入[3,12]则寻找位置
                                                            3        12
再对两个数组进行操作
输出时则按序遍历两个数组即可
时间复杂度O(logn)，主要开销在查找 插入位置
空间复杂度O(2*n)，主要开销在新建两个数组保存range的左右端点
=end 
class RangeList
    def initialize
        @left = Array.new
        @right = Array.new
    end

    #新增range
    def add(range)
        # 当前rangelist空，直接在末尾插入
        if @left.empty? and @right.empty?
            @left.push(range[0])
            @right.push(range[1])

        # 不为空则寻找位置进行操作
        else
            leftIdx,rightIdx = findLoc(range)
            # 现有range与新增的range不相交
            if (leftIdx == rightIdx) && ((leftIdx==0) || (leftIdx==@left.length) || (@right[rightIdx-1]<range[0] && @left[leftIdx]>range[1]))
                @left.insert(leftIdx,range[0])
                @right.insert(rightIdx,range[1])

            # 现有range与新增range的左半部分相交
            elsif (@right[rightIdx] > range[0]) && (@right[rightIdx] < range[1]) && leftIdx-1==rightIdx
                @right[rightIdx] = range[1]

            # 现有range与新增range的右半部分相交
            elsif (@left[rightIdx] < range[1]) && (@left[rightIdx] > range[0]) && leftIdx-1==rightIdx
                @left[rightIdx] = range[0]

            # 新增range连接现有range左右
            elsif (@right[rightIdx] == range[0]) && (@left[leftIdx] == range[1]) && leftIdx-1==rightIdx
                @right[rightIdx] = @right[leftIdx]
                @right[rightIdx+1] = nil
                @left[rightIdx+1] = nil
            
            # 现有range与新增range相交，删除覆盖掉的部分
            elsif 
                leftIdx -= 1
                # 新增range右端覆盖了所有
                if leftIdx == @left.length-1
                    @right[rightIdx] = range[1]
                else
                    @right[rightIdx] = @right[leftIdx]
                end
                # 新增range左端覆盖了所有
                if rightIdx == 0
                    @left[rightIdx] = range[0]
                end
                @right[rightIdx+1,leftIdx-rightIdx] = nil
                @left[rightIdx+1,leftIdx-rightIdx] = nil
            end
        end
        @left.compact!
        @right.compact!
    end

    #移除range
    def remove(range)
        leftIdx,rightIdx = findLoc(range)
        # 如果leftIdx与rightIdx相同则现有range与删除的range不相交，因此只考虑不相同的情况
        if leftIdx != rightIdx
            leftIdx -= 1

            # leftIdx与rightIdx相差1，且左端比操作range左端大，则将左端设为操作range的右端
            if leftIdx == rightIdx && @left[leftIdx]>=range[0]
                @left[leftIdx] = range[1]
                if @right[rightIdx] == range[1]
                    @left[leftIdx] = nil
                    @right[leftIdx] = nil
                end

            # leftIdx与rightIdx相差1，且左端比操作range左端小，则将其分成两半
            elsif leftIdx == rightIdx && @left[leftIdx] < range[0] 
                @left.insert(leftIdx,@left[leftIdx])
                @right.insert(leftIdx,range[0])
                @left[leftIdx+1] = range[1]
                if @right[rightIdx+1] <= range[1]
                    @left[leftIdx+1] = nil
                    @right[leftIdx+1] = nil
                end

            # 相差不止1则说明中间有多个range需要删除，分别修改左右端点并删除
            elsif 
                @right[rightIdx] = range[0]
                @left[leftIdx] = range[1]
                @right[rightIdx+1,leftIdx-rightIdx-1] = nil
                @left[rightIdx+1,leftIdx-rightIdx-1] = nil
                if range[1] >= @right[leftIdx+1]
                    @left[leftIdx+1] = nil
                    @right[leftIdx+1] = nil
                end
            end
        end
        @left.compact!
        @right.compact!
    end

    #按序输出
    def print
        for idx in 0..(@left.length-1)
            printf "[%d,%d) ",@left[idx],@right[idx]
        end
        puts
    end

    #根据需要操作的range寻找位置
    def findLoc(range)
        rightIdx = 0
        rightIdx = binarySearch(@right,0,@right.length-1,range[0])
        leftIdx = rightIdx
        leftIdx = binarySearch(@left,rightIdx,@left.length-1,range[1])
        return leftIdx,rightIdx
    end

    # 二分查找第一个比n大的数
    def binarySearch(arr,low,high,n)
        if low > high
            return low
        end
        mid = (low+high)/2
        if arr[mid] < n && low<=high
            return binarySearch(arr,mid+1,high,n)
        else
            return binarySearch(arr,low,mid-1,n)
        end

    end
end

rl = RangeList.new
rl.add([10, 15])
rl.add([1,5])
rl.add([25,30])
rl.add([45,47])
rl.add([49,50])
rl.add([15,25])
rl.add([27,46])
rl.add([48,50])
rl.add([25,100])
rl.remove([1,5])
rl.remove([50,70])
rl.remove([80,90])
rl.add([0,100])
rl.print
# // Should display: [0, 100) 
# rl.print
# // Should display: [1, 5) [10, 21)
# rl.add([2, 4])
# rl.print
# // Should display: [1, 5) [10, 21)
# rl.add([3, 16])
# rl.print
# # // Should display: [1, 8) [10, 21)
# rl.remove([10, 10])
# rl.print
# # // Should display: [1, 8) [10, 21)
# rl.remove([10, 22])
# rl.print
# # // Should display: [1, 8) [11, 21)
# rl.remove([15, 17])
# rl.print
# # // Should display: [1, 8) [11, 15) [17, 21)
# rl.remove([3, 19])
# rl.print
# // Should display: [1, 3) [19, 21)
